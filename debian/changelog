libstring-tokenizer-perl (0.06-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libstring-tokenizer-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 22:20:24 +0100

libstring-tokenizer-perl (0.06-2) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Ben Webb from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 10:57:42 +0100

libstring-tokenizer-perl (0.06-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 14 Jun 2022 14:28:57 +0200

libstring-tokenizer-perl (0.06-1) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Lucas Kanashiro ]
  * Import upstream version 0.06
  * Add d/u/metadata
  * Drop manpage_spelling.patch, already applied by upstream
  * Update years of upstream copyright
  * d/copyright: drop comma from Upstream-Contact
  * Update Debian packaging copyright
  * Bump debhelper compatibility level to 9
  * Declare compliance with Debian policy 3.9.7
  * Wrap-and-sort dependencies
  * Mark package as autopkgtest-able

  [ gregor herrmann ]
  * Drop build dependencies which are not used anymore.

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Mon, 29 Feb 2016 09:11:51 -0300

libstring-tokenizer-perl (0.05-1) unstable; urgency=low

  * Initial Release. (closes: #636288)

 -- Ben Webb <bjwebb67@googlemail.com>  Tue, 02 Aug 2011 15:04:34 +0000
